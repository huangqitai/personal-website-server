package com.personal.website.eureka.module;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @author hqt
 */
@EnableEurekaServer
@SpringBootApplication
public class PersonalEurekaApplication {

    public static void main(String[] args) {
        SpringApplication.run(PersonalEurekaApplication.class, args);
    }

}
