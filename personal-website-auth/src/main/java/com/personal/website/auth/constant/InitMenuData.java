package com.personal.website.auth.constant;

public class InitMenuData {
    public static String INIT_MENU_DATA = "{\n" +
            "    \"code\": 200,\n" +
            "    \"msg\": \"成功\",\n" +
            "    \"data\": [\n" +
            "        {\n" +
            "            \"path\": \"/home/index\",\n" +
            "            \"name\": \"home\",\n" +
            "            \"component\": \"/home/index\",\n" +
            "            \"meta\": {\n" +
            "                \"icon\": \"HomeFilled\",\n" +
            "                \"title\": \"首页\",\n" +
            "                \"isLink\": \"\",\n" +
            "                \"isHide\": false,\n" +
            "                \"isFull\": false,\n" +
            "                \"isAffix\": true,\n" +
            "                \"isKeepAlive\": true\n" +
            "            }\n" +
            "        },\n" +
            "        {\n" +
            "            \"path\": \"/dataScreen\",\n" +
            "            \"name\": \"dataScreen\",\n" +
            "            \"component\": \"/dataScreen/index\",\n" +
            "            \"meta\": {\n" +
            "                \"icon\": \"Histogram\",\n" +
            "                \"title\": \"数据大屏\",\n" +
            "                \"isLink\": \"\",\n" +
            "                \"isHide\": false,\n" +
            "                \"isFull\": true,\n" +
            "                \"isAffix\": false,\n" +
            "                \"isKeepAlive\": true\n" +
            "            }\n" +
            "        },\n" +
            "        {\n" +
            "            \"path\": \"/proTable\",\n" +
            "            \"name\": \"proTable\",\n" +
            "            \"redirect\": \"/proTable/useProTable\",\n" +
            "            \"meta\": {\n" +
            "                \"icon\": \"MessageBox\",\n" +
            "                \"title\": \"超级表格\",\n" +
            "                \"isLink\": \"\",\n" +
            "                \"isHide\": false,\n" +
            "                \"isFull\": false,\n" +
            "                \"isAffix\": false,\n" +
            "                \"isKeepAlive\": true\n" +
            "            },\n" +
            "            \"children\": [\n" +
            "                {\n" +
            "                    \"path\": \"/proTable/useProTable\",\n" +
            "                    \"name\": \"useProTable\",\n" +
            "                    \"component\": \"/proTable/useProTable/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"使用 ProTable\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    },\n" +
            "                    \"children\": [\n" +
            "                        {\n" +
            "                            \"path\": \"/proTable/useProTable/detail/:id\",\n" +
            "                            \"name\": \"useProTableDetail\",\n" +
            "                            \"component\": \"/proTable/useProTable/detail\",\n" +
            "                            \"meta\": {\n" +
            "                                \"icon\": \"Menu\",\n" +
            "                                \"title\": \"ProTable 详情\",\n" +
            "                                \"isLink\": \"\",\n" +
            "                                \"isHide\": true,\n" +
            "                                \"isFull\": false,\n" +
            "                                \"isAffix\": false,\n" +
            "                                \"isKeepAlive\": true,\n" +
            "                                \"activeMenu\": \"/proTable/useProTable\"\n" +
            "                            }\n" +
            "                        }\n" +
            "                    ]\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/proTable/useTreeFilter\",\n" +
            "                    \"name\": \"useTreeFilter\",\n" +
            "                    \"component\": \"/proTable/useTreeFilter/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"使用 TreeFilter\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/proTable/useTreeFilter/detail/:id\",\n" +
            "                    \"name\": \"useTreeFilterDetail\",\n" +
            "                    \"component\": \"/proTable/useTreeFilter/detail\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"TreeFilter 详情\",\n" +
            "                        \"activeMenu\": \"/proTable/useTreeFilter\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": true,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/proTable/useSelectFilter\",\n" +
            "                    \"name\": \"useSelectFilter\",\n" +
            "                    \"component\": \"/proTable/useSelectFilter/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"使用 SelectFilter\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/proTable/treeProTable\",\n" +
            "                    \"name\": \"treeProTable\",\n" +
            "                    \"component\": \"/proTable/treeProTable/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"树形 ProTable\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/proTable/complexProTable\",\n" +
            "                    \"name\": \"complexProTable\",\n" +
            "                    \"component\": \"/proTable/complexProTable/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"复杂 ProTable\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/proTable/document\",\n" +
            "                    \"name\": \"proTableDocument\",\n" +
            "                    \"component\": \"/proTable/document/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"ProTable 文档\",\n" +
            "                        \"isLink\": \"https://juejin.cn/post/7166068828202336263/#heading-14\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                }\n" +
            "            ]\n" +
            "        },\n" +
            "        {\n" +
            "            \"path\": \"/auth\",\n" +
            "            \"name\": \"auth\",\n" +
            "            \"redirect\": \"/auth/menu\",\n" +
            "            \"meta\": {\n" +
            "                \"icon\": \"Lock\",\n" +
            "                \"title\": \"权限管理\",\n" +
            "                \"isLink\": \"\",\n" +
            "                \"isHide\": false,\n" +
            "                \"isFull\": false,\n" +
            "                \"isAffix\": false,\n" +
            "                \"isKeepAlive\": true\n" +
            "            },\n" +
            "            \"children\": [\n" +
            "                {\n" +
            "                    \"path\": \"/auth/menu\",\n" +
            "                    \"name\": \"authMenu\",\n" +
            "                    \"component\": \"/auth/menu/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"菜单权限\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/auth/button\",\n" +
            "                    \"name\": \"authButton\",\n" +
            "                    \"component\": \"/auth/button/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"按钮权限\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                }\n" +
            "            ]\n" +
            "        },\n" +
            "        {\n" +
            "            \"path\": \"/assembly\",\n" +
            "            \"name\": \"assembly\",\n" +
            "            \"redirect\": \"/assembly/guide\",\n" +
            "            \"meta\": {\n" +
            "                \"icon\": \"Briefcase\",\n" +
            "                \"title\": \"常用组件\",\n" +
            "                \"isLink\": \"\",\n" +
            "                \"isHide\": false,\n" +
            "                \"isFull\": false,\n" +
            "                \"isAffix\": false,\n" +
            "                \"isKeepAlive\": true\n" +
            "            },\n" +
            "            \"children\": [\n" +
            "                {\n" +
            "                    \"path\": \"/assembly/guide\",\n" +
            "                    \"name\": \"guide\",\n" +
            "                    \"component\": \"/assembly/guide/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"引导页\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/assembly/tabs\",\n" +
            "                    \"name\": \"tabs\",\n" +
            "                    \"component\": \"/assembly/tabs/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"标签页操作\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    },\n" +
            "                    \"children\": [\n" +
            "                        {\n" +
            "                            \"path\": \"/assembly/tabs/detail/:id\",\n" +
            "                            \"name\": \"tabsDetail\",\n" +
            "                            \"component\": \"/assembly/tabs/detail\",\n" +
            "                            \"meta\": {\n" +
            "                                \"icon\": \"Menu\",\n" +
            "                                \"title\": \"Tab 详情\",\n" +
            "                                \"activeMenu\": \"/assembly/tabs\",\n" +
            "                                \"isLink\": \"\",\n" +
            "                                \"isHide\": true,\n" +
            "                                \"isFull\": false,\n" +
            "                                \"isAffix\": false,\n" +
            "                                \"isKeepAlive\": true\n" +
            "                            }\n" +
            "                        }\n" +
            "                    ]\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/assembly/selectIcon\",\n" +
            "                    \"name\": \"selectIcon\",\n" +
            "                    \"component\": \"/assembly/selectIcon/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"图标选择器\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/assembly/selectFilter\",\n" +
            "                    \"name\": \"selectFilter\",\n" +
            "                    \"component\": \"/assembly/selectFilter/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"分类筛选器\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/assembly/treeFilter\",\n" +
            "                    \"name\": \"treeFilter\",\n" +
            "                    \"component\": \"/assembly/treeFilter/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"树形筛选器\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/assembly/svgIcon\",\n" +
            "                    \"name\": \"svgIcon\",\n" +
            "                    \"component\": \"/assembly/svgIcon/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"SVG 图标\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/assembly/uploadFile\",\n" +
            "                    \"name\": \"uploadFile\",\n" +
            "                    \"component\": \"/assembly/uploadFile/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"文件上传\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/assembly/batchImport\",\n" +
            "                    \"name\": \"batchImport\",\n" +
            "                    \"component\": \"/assembly/batchImport/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"批量添加数据\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/assembly/wangEditor\",\n" +
            "                    \"name\": \"wangEditor\",\n" +
            "                    \"component\": \"/assembly/wangEditor/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"富文本编辑器\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/assembly/draggable\",\n" +
            "                    \"name\": \"draggable\",\n" +
            "                    \"component\": \"/assembly/draggable/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"拖拽组件\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                }\n" +
            "            ]\n" +
            "        },\n" +
            "        {\n" +
            "            \"path\": \"/dashboard\",\n" +
            "            \"name\": \"dashboard\",\n" +
            "            \"redirect\": \"/dashboard/dataVisualize\",\n" +
            "            \"meta\": {\n" +
            "                \"icon\": \"Odometer\",\n" +
            "                \"title\": \"Dashboard\",\n" +
            "                \"isLink\": \"\",\n" +
            "                \"isHide\": false,\n" +
            "                \"isFull\": false,\n" +
            "                \"isAffix\": false,\n" +
            "                \"isKeepAlive\": true\n" +
            "            },\n" +
            "            \"children\": [\n" +
            "                {\n" +
            "                    \"path\": \"/dashboard/dataVisualize\",\n" +
            "                    \"name\": \"dataVisualize\",\n" +
            "                    \"component\": \"/dashboard/dataVisualize/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"数据可视化\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                }\n" +
            "            ]\n" +
            "        },\n" +
            "        {\n" +
            "            \"path\": \"/form\",\n" +
            "            \"name\": \"form\",\n" +
            "            \"redirect\": \"/form/proForm\",\n" +
            "            \"meta\": {\n" +
            "                \"icon\": \"Tickets\",\n" +
            "                \"title\": \"表单 Form\",\n" +
            "                \"isLink\": \"\",\n" +
            "                \"isHide\": false,\n" +
            "                \"isFull\": false,\n" +
            "                \"isAffix\": false,\n" +
            "                \"isKeepAlive\": true\n" +
            "            },\n" +
            "            \"children\": [\n" +
            "                {\n" +
            "                    \"path\": \"/form/proForm\",\n" +
            "                    \"name\": \"proForm\",\n" +
            "                    \"component\": \"/form/proForm/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"超级 Form\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/form/basicForm\",\n" +
            "                    \"name\": \"basicForm\",\n" +
            "                    \"component\": \"/form/basicForm/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"基础 Form\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/form/validateForm\",\n" +
            "                    \"name\": \"validateForm\",\n" +
            "                    \"component\": \"/form/validateForm/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"校验 Form\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/form/dynamicForm\",\n" +
            "                    \"name\": \"dynamicForm\",\n" +
            "                    \"component\": \"/form/dynamicForm/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"动态 Form\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                }\n" +
            "            ]\n" +
            "        },\n" +
            "        {\n" +
            "            \"path\": \"/echarts\",\n" +
            "            \"name\": \"echarts\",\n" +
            "            \"redirect\": \"/echarts/waterChart\",\n" +
            "            \"meta\": {\n" +
            "                \"icon\": \"TrendCharts\",\n" +
            "                \"title\": \"ECharts\",\n" +
            "                \"isLink\": \"\",\n" +
            "                \"isHide\": false,\n" +
            "                \"isFull\": false,\n" +
            "                \"isAffix\": false,\n" +
            "                \"isKeepAlive\": true\n" +
            "            },\n" +
            "            \"children\": [\n" +
            "                {\n" +
            "                    \"path\": \"/echarts/waterChart\",\n" +
            "                    \"name\": \"waterChart\",\n" +
            "                    \"component\": \"/echarts/waterChart/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"水型图\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/echarts/columnChart\",\n" +
            "                    \"name\": \"columnChart\",\n" +
            "                    \"component\": \"/echarts/columnChart/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"柱状图\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/echarts/lineChart\",\n" +
            "                    \"name\": \"lineChart\",\n" +
            "                    \"component\": \"/echarts/lineChart/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"折线图\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/echarts/pieChart\",\n" +
            "                    \"name\": \"pieChart\",\n" +
            "                    \"component\": \"/echarts/pieChart/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"饼图\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/echarts/radarChart\",\n" +
            "                    \"name\": \"radarChart\",\n" +
            "                    \"component\": \"/echarts/radarChart/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"雷达图\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/echarts/nestedChart\",\n" +
            "                    \"name\": \"nestedChart\",\n" +
            "                    \"component\": \"/echarts/nestedChart/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"嵌套环形图\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                }\n" +
            "            ]\n" +
            "        },\n" +
            "        {\n" +
            "            \"path\": \"/directives\",\n" +
            "            \"name\": \"directives\",\n" +
            "            \"redirect\": \"/directives/copyDirect\",\n" +
            "            \"meta\": {\n" +
            "                \"icon\": \"Stamp\",\n" +
            "                \"title\": \"自定义指令\",\n" +
            "                \"isLink\": \"\",\n" +
            "                \"isHide\": false,\n" +
            "                \"isFull\": false,\n" +
            "                \"isAffix\": false,\n" +
            "                \"isKeepAlive\": true\n" +
            "            },\n" +
            "            \"children\": [\n" +
            "                {\n" +
            "                    \"path\": \"/directives/copyDirect\",\n" +
            "                    \"name\": \"copyDirect\",\n" +
            "                    \"component\": \"/directives/copyDirect/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"复制指令\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/directives/watermarkDirect\",\n" +
            "                    \"name\": \"watermarkDirect\",\n" +
            "                    \"component\": \"/directives/watermarkDirect/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"水印指令\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/directives/dragDirect\",\n" +
            "                    \"name\": \"dragDirect\",\n" +
            "                    \"component\": \"/directives/dragDirect/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"拖拽指令\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/directives/debounceDirect\",\n" +
            "                    \"name\": \"debounceDirect\",\n" +
            "                    \"component\": \"/directives/debounceDirect/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"防抖指令\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/directives/throttleDirect\",\n" +
            "                    \"name\": \"throttleDirect\",\n" +
            "                    \"component\": \"/directives/throttleDirect/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"节流指令\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/directives/longpressDirect\",\n" +
            "                    \"name\": \"longpressDirect\",\n" +
            "                    \"component\": \"/directives/longpressDirect/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"长按指令\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                }\n" +
            "            ]\n" +
            "        },\n" +
            "        {\n" +
            "            \"path\": \"/menu\",\n" +
            "            \"name\": \"menu\",\n" +
            "            \"redirect\": \"/menu/menu1\",\n" +
            "            \"meta\": {\n" +
            "                \"icon\": \"List\",\n" +
            "                \"title\": \"菜单嵌套\",\n" +
            "                \"isLink\": \"\",\n" +
            "                \"isHide\": false,\n" +
            "                \"isFull\": false,\n" +
            "                \"isAffix\": false,\n" +
            "                \"isKeepAlive\": true\n" +
            "            },\n" +
            "            \"children\": [\n" +
            "                {\n" +
            "                    \"path\": \"/menu/menu1\",\n" +
            "                    \"name\": \"menu1\",\n" +
            "                    \"component\": \"/menu/menu1/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"菜单1\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/menu/menu2\",\n" +
            "                    \"name\": \"menu2\",\n" +
            "                    \"redirect\": \"/menu/menu2/menu21\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"菜单2\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    },\n" +
            "                    \"children\": [\n" +
            "                        {\n" +
            "                            \"path\": \"/menu/menu2/menu21\",\n" +
            "                            \"name\": \"menu21\",\n" +
            "                            \"component\": \"/menu/menu2/menu21/index\",\n" +
            "                            \"meta\": {\n" +
            "                                \"icon\": \"Menu\",\n" +
            "                                \"title\": \"菜单2-1\",\n" +
            "                                \"isLink\": \"\",\n" +
            "                                \"isHide\": false,\n" +
            "                                \"isFull\": false,\n" +
            "                                \"isAffix\": false,\n" +
            "                                \"isKeepAlive\": true\n" +
            "                            }\n" +
            "                        },\n" +
            "                        {\n" +
            "                            \"path\": \"/menu/menu2/menu22\",\n" +
            "                            \"name\": \"menu22\",\n" +
            "                            \"redirect\": \"/menu/menu2/menu22/menu221\",\n" +
            "                            \"meta\": {\n" +
            "                                \"icon\": \"Menu\",\n" +
            "                                \"title\": \"菜单2-2\",\n" +
            "                                \"isLink\": \"\",\n" +
            "                                \"isHide\": false,\n" +
            "                                \"isFull\": false,\n" +
            "                                \"isAffix\": false,\n" +
            "                                \"isKeepAlive\": true\n" +
            "                            },\n" +
            "                            \"children\": [\n" +
            "                                {\n" +
            "                                    \"path\": \"/menu/menu2/menu22/menu221\",\n" +
            "                                    \"name\": \"menu221\",\n" +
            "                                    \"component\": \"/menu/menu2/menu22/menu221/index\",\n" +
            "                                    \"meta\": {\n" +
            "                                        \"icon\": \"Menu\",\n" +
            "                                        \"title\": \"菜单2-2-1\",\n" +
            "                                        \"isLink\": \"\",\n" +
            "                                        \"isHide\": false,\n" +
            "                                        \"isFull\": false,\n" +
            "                                        \"isAffix\": false,\n" +
            "                                        \"isKeepAlive\": true\n" +
            "                                    }\n" +
            "                                },\n" +
            "                                {\n" +
            "                                    \"path\": \"/menu/menu2/menu22/menu222\",\n" +
            "                                    \"name\": \"menu222\",\n" +
            "                                    \"component\": \"/menu/menu2/menu22/menu222/index\",\n" +
            "                                    \"meta\": {\n" +
            "                                        \"icon\": \"Menu\",\n" +
            "                                        \"title\": \"菜单2-2-2\",\n" +
            "                                        \"isLink\": \"\",\n" +
            "                                        \"isHide\": false,\n" +
            "                                        \"isFull\": false,\n" +
            "                                        \"isAffix\": false,\n" +
            "                                        \"isKeepAlive\": true\n" +
            "                                    }\n" +
            "                                }\n" +
            "                            ]\n" +
            "                        },\n" +
            "                        {\n" +
            "                            \"path\": \"/menu/menu2/menu23\",\n" +
            "                            \"name\": \"menu23\",\n" +
            "                            \"component\": \"/menu/menu2/menu23/index\",\n" +
            "                            \"meta\": {\n" +
            "                                \"icon\": \"Menu\",\n" +
            "                                \"title\": \"菜单2-3\",\n" +
            "                                \"isLink\": \"\",\n" +
            "                                \"isHide\": false,\n" +
            "                                \"isFull\": false,\n" +
            "                                \"isAffix\": false,\n" +
            "                                \"isKeepAlive\": true\n" +
            "                            }\n" +
            "                        }\n" +
            "                    ]\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/menu/menu3\",\n" +
            "                    \"name\": \"menu3\",\n" +
            "                    \"component\": \"/menu/menu3/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"菜单3\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                }\n" +
            "            ]\n" +
            "        },\n" +
            "        {\n" +
            "            \"path\": \"/system\",\n" +
            "            \"name\": \"system\",\n" +
            "            \"redirect\": \"/system/accountManage\",\n" +
            "            \"meta\": {\n" +
            "                \"icon\": \"Tools\",\n" +
            "                \"title\": \"系统管理\",\n" +
            "                \"isLink\": \"\",\n" +
            "                \"isHide\": false,\n" +
            "                \"isFull\": false,\n" +
            "                \"isAffix\": false,\n" +
            "                \"isKeepAlive\": true\n" +
            "            },\n" +
            "            \"children\": [\n" +
            "                {\n" +
            "                    \"path\": \"/system/accountManage\",\n" +
            "                    \"name\": \"accountManage\",\n" +
            "                    \"component\": \"/system/accountManage/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"账号管理\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/system/roleManage\",\n" +
            "                    \"name\": \"roleManage\",\n" +
            "                    \"component\": \"/system/roleManage/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"角色管理\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/system/menuMange\",\n" +
            "                    \"name\": \"menuMange\",\n" +
            "                    \"component\": \"/system/menuMange/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"菜单管理\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/system/departmentManage\",\n" +
            "                    \"name\": \"departmentManage\",\n" +
            "                    \"component\": \"/system/departmentManage/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"部门管理\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/system/dictManage\",\n" +
            "                    \"name\": \"dictManage\",\n" +
            "                    \"component\": \"/system/dictManage/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"字典管理\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/system/timingTask\",\n" +
            "                    \"name\": \"timingTask\",\n" +
            "                    \"component\": \"/system/timingTask/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"定时任务\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/system/systemLog\",\n" +
            "                    \"name\": \"systemLog\",\n" +
            "                    \"component\": \"/system/systemLog/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"系统日志\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                }\n" +
            "            ]\n" +
            "        },\n" +
            "        {\n" +
            "            \"path\": \"/link\",\n" +
            "            \"name\": \"link\",\n" +
            "            \"redirect\": \"/link/bing\",\n" +
            "            \"meta\": {\n" +
            "                \"icon\": \"Paperclip\",\n" +
            "                \"title\": \"外部链接\",\n" +
            "                \"isLink\": \"\",\n" +
            "                \"isHide\": false,\n" +
            "                \"isFull\": false,\n" +
            "                \"isAffix\": false,\n" +
            "                \"isKeepAlive\": true\n" +
            "            },\n" +
            "            \"children\": [\n" +
            "                {\n" +
            "                    \"path\": \"/link/bing\",\n" +
            "                    \"name\": \"bing\",\n" +
            "                    \"component\": \"/link/bing/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"Bing 内嵌\",\n" +
            "                        \"isLink\": \"\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/link/gitee\",\n" +
            "                    \"name\": \"gitee\",\n" +
            "                    \"component\": \"/link/gitee/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"Gitee 仓库\",\n" +
            "                        \"isLink\": \"https://gitee.com/HalseySpicy/Geeker-Admin\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/link/github\",\n" +
            "                    \"name\": \"github\",\n" +
            "                    \"component\": \"/link/github/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"GitHub 仓库\",\n" +
            "                        \"isLink\": \"https://github.com/HalseySpicy/Geeker-Admin\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/link/docs\",\n" +
            "                    \"name\": \"docs\",\n" +
            "                    \"component\": \"/link/docs/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"项目文档\",\n" +
            "                        \"isLink\": \"https://docs.spicyboy.cn\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                },\n" +
            "                {\n" +
            "                    \"path\": \"/link/juejin\",\n" +
            "                    \"name\": \"juejin\",\n" +
            "                    \"component\": \"/link/juejin/index\",\n" +
            "                    \"meta\": {\n" +
            "                        \"icon\": \"Menu\",\n" +
            "                        \"title\": \"掘金主页\",\n" +
            "                        \"isLink\": \"https://juejin.cn/user/3263814531551816/posts\",\n" +
            "                        \"isHide\": false,\n" +
            "                        \"isFull\": false,\n" +
            "                        \"isAffix\": false,\n" +
            "                        \"isKeepAlive\": true\n" +
            "                    }\n" +
            "                }\n" +
            "            ]\n" +
            "        },\n" +
            "        {\n" +
            "            \"path\": \"/about/index\",\n" +
            "            \"name\": \"about\",\n" +
            "            \"component\": \"/about/index\",\n" +
            "            \"meta\": {\n" +
            "                \"icon\": \"InfoFilled\",\n" +
            "                \"title\": \"关于项目\",\n" +
            "                \"isLink\": \"\",\n" +
            "                \"isHide\": false,\n" +
            "                \"isFull\": false,\n" +
            "                \"isAffix\": false,\n" +
            "                \"isKeepAlive\": true\n" +
            "            }\n" +
            "        }\n" +
            "    ]\n" +
            "}";
}
