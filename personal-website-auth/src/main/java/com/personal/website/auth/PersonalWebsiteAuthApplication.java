package com.personal.website.auth;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.FilterType;

@SpringBootApplication
@MapperScan("com.personal.website.auth.mapper")
@ComponentScan(value = {"com.personal.website"})
@EnableDiscoveryClient
public class PersonalWebsiteAuthApplication {

    public static void main(String[] args) {
        SpringApplication.run(PersonalWebsiteAuthApplication.class, args);
    }

}
