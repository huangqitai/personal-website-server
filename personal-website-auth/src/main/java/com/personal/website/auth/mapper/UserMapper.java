package com.personal.website.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.personal.website.auth.entity.User;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 黄启太
 * @since 2023-06-28
 */
@Repository
@Mapper
public interface UserMapper extends BaseMapper<User> {

}
