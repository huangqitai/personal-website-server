package com.personal.website.auth.controller;

import com.alibaba.fastjson.JSONObject;
import com.personal.website.auth.entity.Menu;
import com.personal.website.auth.entity.User;
import com.personal.website.auth.service.IMenuService;
import com.personal.website.publictools.vo.ReturnVo;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Api(tags = "菜单权限中心")
@RestController
@RequestMapping("/auth/menu")
public class MenuController {

    @Autowired
    private IMenuService menuService;

    @RequestMapping("/init")
    public ReturnVo<String> init(){
        return menuService.init();
    }

    @RequestMapping("/list")
    public ReturnVo<List<Menu>>menuList(){
        return menuService.menuList();
    }

    @RequestMapping("/buttons")
    public ReturnVo<JSONObject>buttons(){
        return menuService.buttons();
    }

}
