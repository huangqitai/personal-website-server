package com.personal.website.auth.controller;

import com.personal.website.auth.entity.LoginBean;
import com.personal.website.auth.entity.User;
import com.personal.website.auth.service.IUserService;
import com.personal.website.publictools.vo.ReturnVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

/**
 * 认证控制器
 */
@Api(tags = "认证中心")
@RestController
@RequestMapping("/auth")
public class AuthController {
    @Autowired
    private IUserService userService;

    @ApiOperation("登录")
    @PostMapping("/login")
    public ReturnVo<Object> login(@RequestBody LoginBean loginBean, HttpServletRequest request){
        return userService.login(loginBean);
    }

    @PostMapping("/register")
    public ReturnVo<String> register(@Validated @RequestBody User user){
        return userService.register(user);
    }

    @GetMapping("/getUserinfo")
    public ReturnVo<Object> getUserinfo(){
        return ReturnVo.success();
    }
}
