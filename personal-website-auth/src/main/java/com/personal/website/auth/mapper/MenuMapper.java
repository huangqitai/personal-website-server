package com.personal.website.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.personal.website.auth.entity.Menu;
import org.apache.ibatis.annotations.Mapper;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 菜单表 Mapper 接口
 * </p>
 *
 * @author 黄启太
 * @since 2024-03-12
 */
@Repository
@Mapper
public interface MenuMapper extends BaseMapper<Menu> {

}
