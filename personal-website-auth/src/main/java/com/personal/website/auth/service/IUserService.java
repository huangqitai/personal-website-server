package com.personal.website.auth.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.personal.website.auth.entity.LoginBean;
import com.personal.website.auth.entity.User;
import com.personal.website.publictools.vo.ReturnVo;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 黄启太
 * @since 2023-06-28
 */
public interface IUserService extends IService<User> {

    ReturnVo<String> register(User user);

    ReturnVo<Object> login(LoginBean loginBean);

    User findUserByAccont(String accont);
}
