package com.personal.website.auth.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.IService;
import com.personal.website.auth.entity.Menu;
import com.personal.website.publictools.vo.ReturnVo;

import java.util.List;

/**
 * <p>
 * 菜单表 服务类
 * </p>
 *
 * @author 黄启太
 * @since 2024-03-12
 */
public interface IMenuService extends IService<Menu> {

    ReturnVo<String> init();

    ReturnVo<List<Menu>> menuList();

    ReturnVo<JSONObject> buttons();
}
