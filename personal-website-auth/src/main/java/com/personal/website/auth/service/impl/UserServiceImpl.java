package com.personal.website.auth.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.personal.website.auth.entity.LoginBean;
import com.personal.website.auth.entity.User;
import com.personal.website.auth.mapper.UserMapper;
import com.personal.website.auth.service.IUserService;
import com.personal.website.auth.utils.PasswordUtils;
import com.personal.website.jwt.utils.JwtTokenUtils;
import com.personal.website.publictools.enums.ResultEnum;
import com.personal.website.publictools.enums.UserStatusEnum;
import com.personal.website.publictools.vo.ReturnVo;
import com.personal.website.redis.utils.RedisUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 黄启太
 * @since 2023-06-28
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {
    @Autowired
    private UserMapper userMapper;

    @Autowired
    private RedisUtil redisUtil;

    @Override
    public ReturnVo<String> register(User user) {
        User old = findUserByAccont(user.getUserAccont());
        if (old!=null){
            return ReturnVo.fail("该账号已存在，本系统账号唯一");
        }
        user.setUserId(UUID.randomUUID().toString());
        user.setSalt(user.getUserAccont());
        user.setStatus(UserStatusEnum.VALID.getCode());
        user.setUserPassword(PasswordUtils.encode(user.getUserPassword(),user.getUserAccont()));
        userMapper.insert(user);
        return ReturnVo.success("注册成功");
    }

    @Override
    public ReturnVo<Object> login(LoginBean loginBean) {
        String account = loginBean.getAccount();
        String password = loginBean.getPassword();
        /*String captcha = loginBean.getCaptcha();
        String kaptcha = "xxx";
        *//*if(kaptcha == null){
            System.out.println("验证码已失效");
        }*//*
        if(!captcha.equals(kaptcha)){
            System.out.println("验证码不正确");
        }*/
        // 用户信息
        User user = findUserByAccont(account);
        // 账号不存在、密码错误
        if (user == null) {
            return new ReturnVo<>(ResultEnum.AUTHFAIL.getCode(),ResultEnum.AUTHFAIL.getMsg()+"：账号不存在");
        }
        if (!PasswordUtils.matches(user.getSalt(), password, user.getUserPassword())) {
            return new ReturnVo<>(ResultEnum.AUTHFAIL.getCode(),ResultEnum.AUTHFAIL.getMsg()+"：密码错误");
        }
       /* // 账号锁定
        if (user.getStatus() == 0) {
            return new ReturnVo(ResultEnum.AUTHFAIL.getCode(),ResultEnum.AUTHFAIL.getMsg()+"：用户锁定");
        }*/
        Map<String, Object> claims = new HashMap<>();
        claims.put("account",account);
        claims.put("username",user.getUserName());
        claims.put("userid",user.getUserId());
        claims.put("userlevel",user.getLevel());
        Map<String, Object> resultMap = JwtTokenUtils.generateTokenAndRefreshToken(claims);
        resultMap.put("username",user.getUserName());
        redisUtil.set(JwtTokenUtils.REFRESH_TOKEN+"_"+user.getUserId(),resultMap.get(JwtTokenUtils.REFRESH_TOKEN),JwtTokenUtils.EXPIRE_TIME, TimeUnit.MILLISECONDS);
        return ReturnVo.token(resultMap);
    }

    @Override
    public User findUserByAccont(String accont) {
        //创建条件构造器对象
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq("USER_ACCONT",accont);
        //执行查询
        return baseMapper.selectOne(queryWrapper);
    }
}
