package com.personal.website.auth.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;

/**
 * <p>
 * 菜单表
 * </p>
 *
 * @author 黄启太
 * @since 2024-03-12
 */
@Getter
@Setter
@TableName("P_MENU")
@ApiModel(value = "Menu对象", description = "菜单表")
public class Menu implements Serializable {

    private static final long serialVersionUID = 1L;

      @ApiModelProperty("主键菜单id")
        private String id;

      @ApiModelProperty("菜单名")
      private String name;

      @ApiModelProperty("菜单地址")
      private String path;

      @ApiModelProperty("组成")
      private String component;

      @ApiModelProperty("重定向地址")
      private String redirect;

      @ApiModelProperty("菜单创建时间")
      private Date createtime;

      @ApiModelProperty("菜单属性配置")
      @TableField(exist = false)
      private MenuMeta meta;

      @ApiModelProperty("子级菜单")
      @TableField(exist = false)
      private List<Menu> children;


}
