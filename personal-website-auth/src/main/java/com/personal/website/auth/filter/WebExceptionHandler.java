package com.personal.website.auth.filter;

import com.personal.website.publictools.enums.ResultEnum;
import com.personal.website.publictools.exception.CustomException;
import com.personal.website.publictools.vo.ReturnVo;
import org.springframework.validation.BindException;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

@ControllerAdvice
public class WebExceptionHandler {

    /*自定义异常处理*/
    @ExceptionHandler(value = CustomException.class)
    @ResponseBody
    public ReturnVo<Object> localHandle(CustomException e){
        return ReturnVo.exception(e.getMessage());
    }

    /*空指针异常处理*/
    @ExceptionHandler(NullPointerException.class)
    @ResponseBody
    public ReturnVo<Object> nullPointHandle(){
        return ReturnVo.exception("发生空指针异常");
    }

    /**
     * 参数校验异常处理
     * @param e
     * @return
     */
    @ExceptionHandler({MethodArgumentNotValidException.class})
    @ResponseBody
    public ReturnVo<Object> methodArgumentNotValidExceptionHandler(MethodArgumentNotValidException e) {
        return new ReturnVo<>(ResultEnum.VALIDATE_ERROR.getCode(), e.getBindingResult().getAllErrors().get(0).getDefaultMessage());
    }
    /*最终异常处理*/
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ReturnVo<Object> exceptionHandle(Exception e){
        e.printStackTrace();
        return ReturnVo.exception(e.getMessage());
    }
}

