package com.personal.website.auth.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 用户信息
 * </p>
 *
 * @author 黄启太
 * @since 2023-06-28
 */
@Data
@TableName("P_USER")
@ApiModel(value = "用户信息", description = "用户信息")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("主键用户id")
    @TableField("USER_ID")
    private String userId;

    @ApiModelProperty("用户名")
    @TableField("USER_NAME")
    @NotNull(message = "用户名不可为空")
    private String userName;

    @ApiModelProperty("用户账号")
    @TableField("USER_ACCONT")
    @NotNull(message = "用户账号不可为空")
    private String userAccont;

    @ApiModelProperty("用户密码")
    @TableField("USER_PASSWORD")
    @NotNull(message = "用户密码不可为空")
    private String userPassword;

    @ApiModelProperty("用户注册时间")
    @TableField("USER_REGTIME")
    private LocalDateTime userRegtime;

    @ApiModelProperty("盐")
    @TableField("SALT")
    private String salt;

    @ApiModelProperty("用户状态")
    @TableField("STATUS")
    private Integer status;
    @ApiModelProperty("用户级别")
    @TableField("LEVEL")
    private Integer level;

}
