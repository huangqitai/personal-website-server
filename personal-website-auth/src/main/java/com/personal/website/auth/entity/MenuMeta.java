package com.personal.website.auth.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

/**
 * <p>
 * 菜单属性表
 * </p>
 *
 * @author 黄启太
 * @since 2024-03-12
 */
@Getter
@Setter
@TableName("P_MENU_META")
@ApiModel(value = "MenuMeta对象", description = "菜单属性表")
public class MenuMeta implements Serializable {

    private static final long serialVersionUID = 1L;

      @ApiModelProperty("主键菜单属性id")
      private String id;

      @ApiModelProperty("菜单ID")
      private String menuId;

      @ApiModelProperty("图标")
      private String icon;

      @ApiModelProperty("是否固定")
      @TableField("ISAFFIX")
      private Boolean isAffix;

      @ApiModelProperty("是否填满")
      @TableField("ISFULL")
      private Boolean isFull;

      @ApiModelProperty("是否隐藏")
      @TableField("ISHIDE")
      private Boolean isHide;

      @ApiModelProperty("是否保持活跃")
      @TableField("ISKEEPALIVE")
      private Boolean isKeepAlive;

      @ApiModelProperty("链接")
      @TableField("ISLINK")
      private String isLink;

      @ApiModelProperty("标题")
      private String title;

      @ApiModelProperty("菜单创建时间")
      private Date createtime;


}
