package com.personal.website.auth.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.personal.website.auth.constant.InitMenuData;
import com.personal.website.auth.entity.Menu;
import com.personal.website.auth.entity.MenuMeta;
import com.personal.website.auth.mapper.MenuMapper;
import com.personal.website.auth.mapper.MenuMetaMapper;
import com.personal.website.auth.service.IMenuService;
import com.personal.website.publictools.utils.UuidUtil;
import com.personal.website.publictools.vo.ReturnVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * <p>
 * 菜单表 服务实现类
 * </p>
 *
 * @author 黄启太
 * @since 2024-03-12
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements IMenuService {

    @Autowired
    private MenuMapper menuMapper;
    @Autowired
    private MenuMetaMapper menuMetaMapper;

    @Transactional
    @Override
    public ReturnVo<String> init() {
        JSONObject data = JSONObject.parseObject(InitMenuData.INIT_MENU_DATA);
        JSONArray menuArray = data.getJSONArray("data");
        for (int i = 0; i < menuArray.size(); i++) {
            JSONObject menuData = menuArray.getJSONObject(i);
            Menu menu = JSONObject.toJavaObject(menuData,Menu.class);
            /*MenuMeta menuMeta = menu.getMeta();
            String menuId = UuidUtil.getUuid();
            menu.setId(menuId);
            menuMeta.setMenuId(menuId);
            menuMeta.setId(UuidUtil.getUuid());
            menuMapper.insert(menu);
            menuMetaMapper.insert(menuMeta);*/
            childrenMenu(menu);
        }
        return ReturnVo.success("初始化菜单成功");
    }

    private void childrenMenu(Menu menu){
        MenuMeta menuMeta = menu.getMeta();
        String menuId = UuidUtil.getUuid();
        menu.setId(menuId);
        menuMeta.setMenuId(menuId);
        menuMeta.setId(UuidUtil.getUuid());
        menuMapper.insert(menu);
        menuMetaMapper.insert(menuMeta);
        if (menu.getChildren()!=null){
            List<Menu> children = menu.getChildren();
            for (Menu childrenMenu:children){
                childrenMenu(childrenMenu);
            }
        }
    }

    @Override
    public ReturnVo<List<Menu>> menuList() {
        List<Menu> menuList = this.list();
        for (Menu menu:menuList){
            MenuMeta menuMeta = menuMetaMapper.selectOneByMenuId(menu.getId());
            menu.setMeta(menuMeta);
        }
        return ReturnVo.success(menuList);
    }

    @Override
    public ReturnVo<JSONObject> buttons() {
        String buttonsDataStr = "{\n" +
                "\t\"useProTable\": [\n" +
                "\t\t\"add\",\n" +
                "\t\t\"batchAdd\",\n" +
                "\t\t\"export\",\n" +
                "\t\t\"batchDelete\",\n" +
                "\t\t\"status\"\n" +
                "\t],\n" +
                "\t\"authButton\": [\n" +
                "\t\t\"add\",\n" +
                "\t\t\"edit\",\n" +
                "\t\t\"delete\",\n" +
                "\t\t\"import\",\n" +
                "\t\t\"export\"\n" +
                "\t]\n" +
                "}";
        JSONObject buttonsData = JSONObject.parseObject(buttonsDataStr);

        return ReturnVo.success(buttonsData);
    }
}
