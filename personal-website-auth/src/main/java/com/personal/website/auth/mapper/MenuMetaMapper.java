package com.personal.website.auth.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.personal.website.auth.entity.MenuMeta;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

/**
 * <p>
 * 菜单属性表 Mapper 接口
 * </p>
 *
 * @author 黄启太
 * @since 2024-03-12
 */
@Repository
@Mapper
public interface MenuMetaMapper extends BaseMapper<MenuMeta> {
    @Select("SELECT * FROM P_MENU_META WHERE MENU_ID = #{menuId}")
    MenuMeta selectOneByMenuId(@Param("menuId") String menuId);

}
