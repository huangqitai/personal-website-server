package com.personal.website.gateway.module.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class GatewayController {

    @RequestMapping("/test")
    public String testController(){
        return "测试接口";
    }
}
