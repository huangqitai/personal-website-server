package com.personal.website.gateway.module;

//import com.ctrip.framework.apollo.spring.annotation.EnableApolloConfig;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
//import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

//@EnableApolloConfig
//@EnableEurekaClient
@ComponentScan(value = {"com.personal.website"})
@EnableDiscoveryClient
@SpringBootApplication
public class PersonalGatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(PersonalGatewayApplication.class, args);
    }

}
