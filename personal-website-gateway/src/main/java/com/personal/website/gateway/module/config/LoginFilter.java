package com.personal.website.gateway.module.config;

import com.alibaba.fastjson.JSONObject;
import com.personal.website.jwt.utils.JwtTokenUtils;
import com.personal.website.publictools.enums.ResultEnum;
import com.personal.website.publictools.vo.ReturnVo;
import com.personal.website.redis.utils.RedisUtil;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.RequestPath;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.HashMap;
import java.util.Map;

@Component
public class LoginFilter  implements GlobalFilter, Ordered {
    @Autowired
    private LoginAllowPathsConfig loginAllowPathsConfig;
    @Autowired
    private RedisUtil redisUtil;
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        ServerHttpRequest request = exchange.getRequest();
        //获取请求的url路径
        RequestPath path = request.getPath();
        if (isAllowPath(path.toString())){
            return chain.filter(exchange);
        }
        String accessToken = request.getHeaders().getFirst("X-Access-Token");
        if (StringUtils.isBlank(accessToken)){
            return loginResponse(exchange,"access_token缺失");
        }
        if (JwtTokenUtils.isTokenExpired(accessToken)){
            return loginResponse(exchange,"access_token失效");
        }
        // 验证 token 里面的 userId 是否为空
        String userId = JwtTokenUtils.getUseridFromToken(accessToken);
        String username = JwtTokenUtils.getUsernameFromToken(accessToken);
        String account = JwtTokenUtils.getAccountFromToken(accessToken);
        String level = JwtTokenUtils.getUserLevelFromToken(accessToken);
        if (StringUtils.isEmpty(userId)) {
            return loginResponse(exchange,"access_token验证失败");
        }
        if (!redisUtil.hasKey(JwtTokenUtils.REFRESH_TOKEN+"_"+userId)){
            return loginResponse(exchange,"access_token已失效，请重新登录");
        }
        else {
            ServerHttpRequest.Builder mutate = request.mutate();
            addHeader(mutate,"userid",userId);
            addHeader(mutate,"username",username);
            addHeader(mutate,"account",account);
            addHeader(mutate,"userlevel",level);
            chain.filter(exchange);
            return chain.filter(exchange);
        }
    }

    @Override
    public int getOrder() {
        return 0;
    }
    public Mono<Void> loginResponse(ServerWebExchange exchange,String msg) {
        ReturnVo<Object> returnVo = new ReturnVo<>(ResultEnum.AUTHFAIL);
        returnVo.setMsg("认证失败："+msg);
        ServerHttpResponse response = exchange.getResponse();
        byte[] bytes = JSONObject.toJSONBytes(returnVo);
        response.getHeaders().add("Content-Type", MediaType.APPLICATION_JSON_VALUE);
        DataBuffer buffer = response.bufferFactory().wrap(bytes);
        response.setStatusCode(HttpStatus.UNAUTHORIZED);
        return response.writeWith(Flux.just(buffer));
    }

    /**
     * 判断当前请求是否是白名单
     * @param path 请求地址
     * @return
     */
    private boolean isAllowPath(String path) {
        //遍历白名单
        for (String allowPath : loginAllowPathsConfig.getAllowPaths()) {
            //判断是否允许
            if(path.startsWith(allowPath)){
                return true;
            }
        }
        return  false;
    }

    /**
     * 添加请求头
     * @param mutate
     * @param name
     * @param value
     */
    private void addHeader(ServerHttpRequest.Builder mutate, String name, Object value) {
        if (value == null) {
            return;
        }
        String valueStr = value.toString();
        String valueEncode = urlEncode(valueStr);
        mutate.header(name, valueEncode);
    }

    private void removeHeader(ServerHttpRequest.Builder mutate, String name) {
        mutate.headers(httpHeaders -> httpHeaders.remove(name)).build();
    }

    /**
     * 内容编码
     *
     * @param str 内容
     * @return 编码后的内容
     */
    public static String urlEncode(String str) {
        try {
            return URLEncoder.encode(str, "UTF-8");
        }
        catch (UnsupportedEncodingException e)
        {
            return StringUtils.EMPTY;
        }
    }
}
