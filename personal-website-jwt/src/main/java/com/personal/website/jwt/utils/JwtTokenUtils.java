package com.personal.website.jwt.utils;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * JWT工具类
 */
public class JwtTokenUtils{


    /**
     * 创建时间
     */
    private static final String CREATED = "created";
    /**
     * 密钥
     */
    private static final String SECRET = "huangqitai";
    /**
     * 有效期12小时
     */
    //private static final long EXPIRE_TIME = 12 * 60 * 60 * 1000;
            //单位是毫秒
    public static final long EXPIRE_TIME = 5 * 60 * 1000;
    public static final String ACCESS_TOKEN = "access_token";
    public static final String REFRESH_TOKEN = "refresh_token";

    /**
     * 生成 token 令牌
     *
     * @param claims 数据声明
     * @return 令token牌
     */

    public static Map<String, Object> generateTokenAndRefreshToken(Map<String, Object> claims) {
        Map<String, Object> tokenMap = new HashMap<String, Object>();
        tokenMap.put(ACCESS_TOKEN,generateToken(claims));
        tokenMap.put(REFRESH_TOKEN,generateRefreshToken(claims));
        return tokenMap;
    }

    /**
     * 从数据声明生成令牌
     *
     * @param claims 数据声明
     * @return 令牌
     */
    public static String generateToken(Map<String, Object> claims) {
        Date expirationDate = new Date(System.currentTimeMillis() + EXPIRE_TIME);
        return Jwts.builder().setClaims(claims).setExpiration(expirationDate).signWith(SignatureAlgorithm.HS512, SECRET).compact();
    }
    /**
     * 生成刷新令牌 refreshToken，有效期是令牌的 2 倍
     *
     * @param claims 数据声明
     * @return 令牌
     */
    public static String generateRefreshToken(Map<String, Object> claims) {
        Date expirationDate = new Date(System.currentTimeMillis() + EXPIRE_TIME * 2);
        return Jwts.builder().setClaims(claims)
                .setExpiration(expirationDate)
                .signWith(SignatureAlgorithm.HS512, SECRET)
                .compact();
    }
    /**
     * 从令牌中获取用户id
     *
     * @param token 令牌
     * @return 用户名
     */
    public static String getUseridFromToken(String token) {
        String userid;
        try {
            Claims claims = getClaimsFromToken(token);
            userid = (String) claims.get("userid");
        } catch (Exception e) {
            userid = null;
        }
        return userid;
    }
    /**
     * 从令牌中获取用户账号
     *
     * @param token 令牌
     * @return 用户名
     */
    public static String getAccountFromToken(String token) {
        String account;
        try {
            Claims claims = getClaimsFromToken(token);
            account = (String) claims.get("account");
        } catch (Exception e) {
            account = null;
        }
        return account;
    }
    /**
     * 从令牌中获取用户名
     *
     * @param token 令牌
     * @return 用户名
     */
    public static String getUsernameFromToken(String token) {
        String username;
        try {
            Claims claims = getClaimsFromToken(token);
            username = (String) claims.get("username");
        } catch (Exception e) {
            username = null;
        }
        return username;
    }
    /**
     * 从令牌中获取用户等级
     *
     * @param token 令牌
     * @return 用户名
     */
    public static String getUserLevelFromToken(String token) {
        String userlevel;
        try {
            Claims claims = getClaimsFromToken(token);
            userlevel = (String) claims.get("userlevel");
        } catch (Exception e) {
            userlevel = null;
        }
        return userlevel;
    }

    /**
     * 从令牌中获取数据声明
     *
     * @param token 令牌
     * @return 数据声明
     */
    private static Claims getClaimsFromToken(String token) {
        Claims claims;
        try {
            claims = Jwts.parser().setSigningKey(SECRET).parseClaimsJws(token).getBody();
        } catch (Exception e) {
            claims = null;
        }
        return claims;
    }

    /**
     * 验证令牌
     * @param token
     * @param username
     * @return
     */
    public static Boolean validateToken(String token, String username) {
        String userName = getUsernameFromToken(token);
        return (userName.equals(username) && !isTokenExpired(token));
    }

    /**
     * 刷新令牌
     * @param token
     * @return
     */
    public static String refreshToken(String token) {
        String refreshedToken;
        try {
            Claims claims = getClaimsFromToken(token);
            claims.put(CREATED, new Date());
            refreshedToken = generateToken(claims);
        } catch (Exception e) {
            refreshedToken = null;
        }
        return refreshedToken;
    }

    /**
     * 判断令牌是否过期
     *
     * @param token 令牌
     * @return 是否过期
     */
    public static Boolean isTokenExpired(String token) {
        try {
            Claims claims = getClaimsFromToken(token);
            Date expiration = claims.getExpiration();
            return expiration.before(new Date());
        } catch (Exception e) {
            //验证异常等同于过期
            return true;
        }
    }

    /**
     * 获取请求token
     * @param request
     * @return
     */
    public static String getToken(HttpServletRequest request) {
        String token = request.getHeader("access_token");
        String tokenHead = "Bearer ";
        if(token == null) {
            token = request.getHeader("token");
        } else if(token.contains(tokenHead)){
            token = token.substring(tokenHead.length());
        }
        if("".equals(token)) {
            token = null;
        }
        return token;
    }

}
