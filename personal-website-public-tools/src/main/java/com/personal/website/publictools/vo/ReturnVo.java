package com.personal.website.publictools.vo;


import com.personal.website.publictools.enums.ResultEnum;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

/**
 * 公共返回参数对象
 */
@Data
public class ReturnVo<T> {
    private Integer code;
    private String msg;
    private T data;

    public ReturnVo(Integer code, String msg, T data) {
        this.code = code;
        this.msg = msg;
        this.data = data;
    }
    public ReturnVo(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
        this.data = null;
    }
    public ReturnVo(ResultEnum resultEnum){
        this.code = resultEnum.getCode();
        this.msg = resultEnum.getMsg();
        this.data = null;
    }
    public static<T> ReturnVo<T> success(){
        return new ReturnVo<T>(ResultEnum.SUCCESS);
    }
    public static<T> ReturnVo<T> success(String msg){
        return new ReturnVo<T>(ResultEnum.SUCCESS.getCode(),msg);
    }
    public static<T> ReturnVo<T> success(T data){
        return new ReturnVo<T>(ResultEnum.SUCCESS.getCode(),ResultEnum.SUCCESS.getMsg(),data);
    }
    public static<T> ReturnVo<T> success(String msg,T data){
        return new ReturnVo<T>(ResultEnum.SUCCESS.getCode(),msg,data);
    }
    public static<T> ReturnVo<T> token(T data){
        return new ReturnVo<T>(ResultEnum.LOGIN_SUCCESS.getCode(),ResultEnum.LOGIN_SUCCESS.getMsg(),data);
    }
    public static<T> ReturnVo<T> fail(){
        return new ReturnVo<T>(ResultEnum.FAIL);
    }
    public static<T> ReturnVo<T> fail(String msg){
        return new ReturnVo<T>(ResultEnum.FAIL.getCode(),msg);
    }
    public static<T> ReturnVo<T> exception(String massage){
        return new ReturnVo<T>(ResultEnum.EXCEPTION.getCode(),ResultEnum.EXCEPTION.getMsg()+":"+massage);
    }
}
