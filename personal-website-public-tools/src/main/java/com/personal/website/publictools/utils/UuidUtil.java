package com.personal.website.publictools.utils;

import java.util.UUID;

public class UuidUtil {

    public static String getUuid(){
        String uuid = UUID.randomUUID().toString();
        return uuid.replace("-","");
    }
}
