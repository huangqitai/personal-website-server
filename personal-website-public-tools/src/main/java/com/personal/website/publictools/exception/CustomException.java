package com.personal.website.publictools.exception;

/**
 * 自定义异常
 */
public class CustomException extends Exception{
    public CustomException() {
    }

    public CustomException(String message) {
        super(message);
    }
}
