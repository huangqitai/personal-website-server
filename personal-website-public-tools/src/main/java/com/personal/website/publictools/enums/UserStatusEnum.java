package com.personal.website.publictools.enums;

public enum UserStatusEnum {
    VALID(1,"有效"),
    INVALID(0,"无效"),
    LOCK(2,"被锁住")
    ;

    private Integer code;
    private String value;

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    UserStatusEnum(Integer code, String value) {

        this.code = code;
        this.value = value;
    }
}
