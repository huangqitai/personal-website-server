package com.personal.website.publictools.enums;

/**
 * 请求结果枚举
 */
public enum ResultEnum {
    LOGIN_SUCCESS(200,"登录认证成功"),
    SUCCESS(200,"请求成功"),
    FAIL(0,"请求失败"),
    EXCEPTION(-1,"请求异常"),
    VALIDATE_ERROR(-2,"参数校验错误"),
    AUTHFAIL(401,"认证失败");

    private Integer code;
    private String msg;

    ResultEnum(Integer code, String msg) {
        this.code = code;
        this.msg = msg;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
